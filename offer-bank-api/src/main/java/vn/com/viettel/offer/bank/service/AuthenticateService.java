package vn.com.viettel.offer.bank.service;

import org.springframework.stereotype.Service;

public interface AuthenticateService {

	public boolean checkAuthen(String userName, String password);
}
