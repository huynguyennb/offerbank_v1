package vn.com.viettel.offer.bank.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import vn.com.viettel.offer.bank.constant.AppConst.LogAction;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD) //on class level
public @interface LogInfo {

	LogAction action() default LogAction.READ;
	
//	String[] tags() default "";
	
	String comment() default "";
	
//	String createdBy() default "Mkyong";
//	
//	String lastModified() default "03/01/2014";

}