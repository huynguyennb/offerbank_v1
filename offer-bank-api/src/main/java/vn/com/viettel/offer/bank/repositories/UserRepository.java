package vn.com.viettel.offer.bank.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.com.viettel.offer.bank.model.User;;

public interface UserRepository extends JpaRepository<User, Long>, UserCustomRepository {
	
	List<User> findAllByUserNameAndPasswordAndIp(String userName, String password, String ip);
}
