package vn.com.viettel.offer.bank.constant;

public final class ModuleConstants {
	private ModuleConstants(){
		
	}
	public static final long OFFER_TYPE = 1;
	public static final long ACTION_TYPE = 2;
	public static final long PRICE_COMPONENT_TYPE = 3;
	public static final long PCBATCH_MODULE_TYPE=101;
	public static final long ACTION_BATCH_TYPE=100;
	
	//constant switch DB
	public static final long PC_DB = 0;
	public static final long VOCS_DB = 1;
	
	//partern
	
	public static final String REGEX_NUMBER = "\\d+";
	
	
	
	
}
