package vn.com.viettel.offer.bank.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.com.viettel.offer.bank.config.PropertiesConfig;
import vn.com.viettel.offer.bank.service.AuthenticateService;


@Service
public class AuthenticateServiceImpl implements AuthenticateService{

	@Autowired
	private PropertiesConfig config;

	@Override
	public boolean checkAuthen(String userName, String password) {
		//now user/password not encrytp. and check normaly
		return config.getAppUserName().equals(userName) && config.getAppPassword().equals(password);
	}

}
