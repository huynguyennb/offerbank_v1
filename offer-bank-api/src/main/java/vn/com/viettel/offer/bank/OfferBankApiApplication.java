package vn.com.viettel.offer.bank;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import vn.com.viettel.offer.bank.config.LogRequestFilter;
import vn.com.viettel.offer.bank.config.PropertiesConfig;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableConfigurationProperties({PropertiesConfig.class})

@SpringBootApplication
public class OfferBankApiApplication {
	
	static Logger log = LoggerFactory.getLogger(OfferBankApiApplication.class);

	public static void main(String[] args) throws UnknownHostException {
		//SpringApplication.run(ProductCatalogApiApplication.class, args);
		SpringApplication app = new SpringApplication(OfferBankApiApplication.class);
        Environment env = app.run(args).getEnvironment();
        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        log.info("\n----------------------------------------------------------\n\t" +
                "Application '{}' is running! Access URLs:\n\t" +
                "Local: \t\t{}://localhost:{}\n\t" +
                "External: \t{}://{}:{}\n\t" +
                "Profile(s): \t{}\n----------------------------------------------------------",
            env.getProperty("spring.application.name"),
            protocol,
            env.getProperty("server.port"),
            protocol,
            InetAddress.getLocalHost().getHostAddress(),
            env.getProperty("server.port"),
            env.getActiveProfiles());
	}
	
    @Bean
    public FilterRegistrationBean loggingFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new LogRequestFilter());
// In case you want the filter to apply to specific URL patterns only
        registration.addUrlPatterns("/api/*");
        return registration;
    }
	
}
