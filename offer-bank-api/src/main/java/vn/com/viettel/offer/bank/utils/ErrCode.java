/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vn.com.viettel.offer.bank.utils;

/**
 *
 * @author tiemnk
 */
public class ErrCode {
    public static long SUCCESS = 0;
    public static long ERROR = 1;
    
    public long errCode;
    public String errDesc;
    public String data;
    
    public ErrCode() {
        errCode = SUCCESS;
        errDesc = "OK";
        
    }
    public ErrCode (long err) {
        errCode = err;
        errDesc = "";
    }
    public ErrCode(long err, String desc) {
        errCode = err;
        errDesc = desc;
    }
    
    
    public void set(long err, String desc) {
        errCode = err;
        errDesc = desc;
    }
    
    public void set(long err) {
        errCode = err;
        errDesc = "";
    }
    
    public void set(String  desc) {
        errCode = ERROR;
        errDesc = desc;
    }
    
    public void clear() {
        errCode = SUCCESS;
        errDesc = "";
        data ="";
    }
    
    public String toString() {        
        return "ErrCode:"+errCode+",Desc='"+ errDesc+"'";
    }

    public void setData(String inputData) {
        data = inputData;
    }

    public boolean isSuccess() {
        return errCode == SUCCESS;
    }
    
   
}
