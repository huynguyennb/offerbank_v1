package vn.com.viettel.offer.bank.constant;

public enum ObjectTypeEnum {
	Offer(1), Action(2), Price(3);

	private int value;

	private ObjectTypeEnum(int value) {
		this.value = value;
	}

	public int value() {
		return this.value;
	}

	public static boolean isContain(int value) {
		boolean flag = false;
		for (ObjectTypeEnum _value : values()) {
			if (_value.value == value) {
				flag = true;
				break;
			}
		}
		return flag;
	}
}
