package vn.com.viettel.offer.bank.resource.response;


public class SegmentOfOfferResponse {

	private String requestId;
	

	public SegmentOfOfferResponse(String requestId) {
		super();
		this.requestId = requestId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	
	
}
