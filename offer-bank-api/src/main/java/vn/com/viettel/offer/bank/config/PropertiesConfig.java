package vn.com.viettel.offer.bank.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties
public class PropertiesConfig {

	@Value("${api.authen.userName}")
	private String appUserName;
	
	@Value("${api.authen.password}")
	private String appPassword;
	
	@Value("${spring.spark.server.className}")
	private String className;
	
	@Value("${spring.spark.server.pathJarFile}")
	private String jarFile; 
	
	@Value("${spring.spark.server.extendPath}")
	private String extendPath; 

	@Value("${spring.spark.server.rootPath}")
	private String rootPath; 
	
	@Value("${spring.spark.server.host}")
	private String host; 
	
	@Value("${spring.spark.server.port}")
	private String port; 

	@Value("${spring.spark.server.timeOut}")
	private String timeOut; 
	
	@Value("${spring.kafka.server.host_port}")
	private String host_port; 
	
	@Value("${spring.kafka.server.topic}")
	private String topic; 
	
	@Value("${spring.spark.server.tableName}")
	private String tableName; 
	
	@Value("${spring.spark.server.delimiter}")
	private String delimiter; 
	
	@Value("${spring.spark.server.columns}")
	private String columns;
	
	@Value("${spring.spark.server.hiveQuery}")
	private String hiveQuery; 
	
	@Value("${spring.spark.server.gottenFields}")
	private String gottenFields; 
	
	@Value("${spring.spark.server.outputPattern}")
	private String outputPattern;
	
	
	public String getAppUserName() {
		return appUserName;
	}

	public void setAppUserName(String appUserName) {
		this.appUserName = appUserName;
	}

	public String getAppPassword() {
		return appPassword;
	}

	public void setAppPassword(String appPassword) {
		this.appPassword = appPassword;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getJarFile() {
		return jarFile;
	}

	public void setJarFile(String jarFile) {
		this.jarFile = jarFile;
	}

	public String getExtendPath() {
		return extendPath;
	}

	public void setExtendPath(String extendPath) {
		this.extendPath = extendPath;
	}

	public String getRootPath() {
		return rootPath;
	}

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(String timeOut) {
		this.timeOut = timeOut;
	}

	public String getHost_port() {
		return host_port;
	}

	public void setHost_port(String host_port) {
		this.host_port = host_port;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getDelimiter() {
		return delimiter;
	}

	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	public String getColumns() {
		return columns;
	}

	public void setColumns(String columns) {
		this.columns = columns;
	}

	public String getHiveQuery() {
		return hiveQuery;
	}

	public void setHiveQuery(String hiveQuery) {
		this.hiveQuery = hiveQuery;
	}

	public String getGottenFields() {
		return gottenFields;
	}

	public void setGottenFields(String gottenFields) {
		this.gottenFields = gottenFields;
	}

	public String getOutputPattern() {
		return outputPattern;
	}

	public void setOutputPattern(String outputPattern) {
		this.outputPattern = outputPattern;
	}
	
	
}
