package vn.com.viettel.offer.bank.config;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import vn.com.viettel.offer.bank.dto.ServiceLog;
import vn.com.viettel.offer.bank.resource.response.BaseResponse;
import vn.com.viettel.offer.bank.service.AuthenticateService;
import vn.com.viettel.offer.bank.utils.DateUtils;
import vn.com.viettel.offer.bank.utils.LogInfo;

@Component
public class RequestInterceptor extends HandlerInterceptorAdapter {

	Logger log = LoggerFactory.getLogger(RequestInterceptor.class);
	
	private static final String HEADER_USERNAME = "USERNAME";
	private static final String HEADER_PASSWORD = "PASSWORD";

	@Autowired
	private AuthenticateService authenticateService;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String userName = request.getHeader(HEADER_USERNAME);
		String password = request.getHeader(HEADER_PASSWORD);
		
		if(!authenticateService.checkAuthen(userName, password)) {
			BaseResponse<Void> responseData = new BaseResponse<>();
			responseData.setErrorCode(BaseResponse.CODE_UNAUTHORIZED);
			responseData.setDescription("Unauthorized");
			ObjectMapper mapper = new ObjectMapper();
			response.setHeader("content-type", "application/json");
			response.getWriter().println(mapper.writeValueAsString(responseData));
			return false;
		}
		
		long startTime = System.currentTimeMillis();
		request.setAttribute("pc-startTime", startTime);
	
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object, ModelAndView model)
			throws Exception {
//		System.out.println("_________________________________________");
//		System.out.println("In postHandle request processing " + "completed by @RestController");
//		System.out.println("_________________________________________");
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object, Exception arg3)
			throws Exception {
//		System.out.println("________________________________________");
//		System.out.println("In afterCompletion Request Completed");
//		System.out.println("________________________________________");

	}
}