package vn.com.viettel.offer.bank.utils;

import vn.com.viettel.offer.bank.resource.response.BaseResponse;

public class ResponseUtils {

	public static <T> BaseResponse<T> createResponse(String code, String description, T t){
		BaseResponse<T> base = new BaseResponse<>();
		base.setData(t);
		base.setDescription(description);
		base.setErrorCode(code);
		return base;
	}
	
}
