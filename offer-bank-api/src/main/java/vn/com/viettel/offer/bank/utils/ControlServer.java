/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vn.com.viettel.offer.bank.utils;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import org.apache.log4j.Logger;

/**
 *
 * @author tiemnk
 */
public class ControlServer {
    
        private final Logger logger = Logger.getLogger(this.getClass());
    
        private int bindingPort;
    
        
        public ControlServer(int bindingPort) {
            this.bindingPort = bindingPort;
        }
        
        public static void main(String[] args) {
        	new ControlServer(9098).initServer();
        }
        
        public void initServer() {
            logger.info("Start server, listen on port:" + bindingPort);
            int clientNumber = 0;
            
            ServerSocket listener = null ;
            try {
                listener = new ServerSocket(bindingPort);
                while (true) {
                    new RemoteCommand(listener.accept(), clientNumber++).start();
                }
            } catch (Exception  e) {
                logger.error(e, e);
            }    finally {
                try {
                    listener.close();
                } catch (IOException ex) {
                    logger.error(ex, ex);
                }
            }

        }
        
        private static class RemoteCommand extends Thread {
            private final Logger logger = Logger.getLogger(this.getClass());
            private Socket socket;
            private int clientNumber;
            

            public RemoteCommand(Socket socket, int clientNumber) {
                this.socket = socket;
                this.clientNumber = clientNumber;
                log("New connection with client# " + clientNumber + " at " + socket);
                
            }

            /**
             * Services this thread's client by first sending the
             * client a welcome message then repeatedly reading strings
             * and sending back the capitalized version of the string.
             */
            public void run() {
                try {

                    // Decorate the streams so we can send characters
                    // and not just bytes.  Ensure output is flushed
                    // after every newline.
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(socket.getInputStream()));
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

                    // Send a welcome message to the client.
                    

                    // Get messages from the client, line by line; return them
                    // capitalized
                    //while (true) {
                    String input = in.readLine();
                    logger.info("INPUT:|"+input+"|");
                    String cmds[] = input.split("\\s+|;");
                    
                    if (cmds[0].equalsIgnoreCase("IMPORT")) {
                        if (cmds[1].matches("\\d+")) {
                             //OCSImport ocsImport = new  OCSImport();
                             long intOfferId = Long.parseLong(cmds[1]);  
                             ErrCode errCode = new ErrCode();
                             if (intOfferId!=88) {    
                                 errCode.set(1001,"intOfferID not found");
                             }                             
                             out.println(errCode.toString());
                             
                        } else {
                            out.println("ErrCode:101,Desc:Invalid value " + cmds[1]);
                        }
                        
                    } else {
                        out.println("ErrCode:100,Desc:'Invalid command'");//input.toUpperCase());
                    }
                    
                } catch (IOException e) {
                    log("Error handling client# " + clientNumber + ": " + e);
                } finally {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        log("Couldn't close a socket, what's going on?");
                    }
                    log("Connection with client# " + clientNumber + " closed");
                }
            }

            /**
             * Logs a simple message.  In this case we just write the
             * message to the server applications standard output.
             */
            private void log(String message) {
                System.out.println(message);
            }
        }

}
