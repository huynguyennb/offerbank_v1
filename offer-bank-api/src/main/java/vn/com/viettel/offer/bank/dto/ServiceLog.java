package vn.com.viettel.offer.bank.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;


/**
 * The persistent class for the service_log database table.
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceLog {

	private Long id;

	
	private String action;

	
	private String comment;

	
	private String request;

	
	private String response;

	private Timestamp sendDate;

	public ServiceLog() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getRequest() {
		return this.request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return this.response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Timestamp getSendDate() {
		return this.sendDate;
	}

	public void setSendDate(Timestamp sendDate) {
		this.sendDate = sendDate;
	}

	@Override
	public String toString() {
		return "ServiceLog [id=" + id + ", action=" + action + ", comment=" + comment + ", request=" + request
				+ ", response=" + response + ", sendDate=" + sendDate + "]";
	}
	

}