package vn.com.viettel.offer.bank.constant;

public enum QueryTypeENum {
    PC_DB(0),VOCS_DB(1);
    
    private int value;
	
	private QueryTypeENum(int value) {
		this.value = value;
	}
	
	public int value() {
        return this.value;
    }
	
	
	public static boolean isContain(int value) {
		boolean flag = false;
		for (QueryTypeENum _value : values()) {
			if (_value.value == value) {
				flag = true;
				break;
			}
		}
		return flag;
	}
}

