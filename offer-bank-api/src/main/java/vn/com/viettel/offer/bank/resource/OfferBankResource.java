package vn.com.viettel.offer.bank.resource;


import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.sun.research.ws.wadl.Request;

import vn.com.viettel.offer.bank.config.PropertiesConfig;
import vn.com.viettel.offer.bank.constant.AppConst.LogAction;
import vn.com.viettel.offer.bank.dto.UserDTO;
import vn.com.viettel.offer.bank.resource.response.BaseResponse;
import vn.com.viettel.offer.bank.resource.response.BestOfferByISDNResponse;
import vn.com.viettel.offer.bank.resource.response.BestOfferByISDNResponse.InfoByOfferId;
import vn.com.viettel.offer.bank.resource.response.BestOfferBySubIdResponse;
import vn.com.viettel.offer.bank.resource.response.SegmentOfOfferResponse;
import vn.com.viettel.offer.bank.service.impl.UserServiceImpl;
import vn.com.viettel.offer.bank.spark.SparkCommandEnvelope;
import vn.com.viettel.offer.bank.spark.SparkCommandPublisher;
import vn.com.viettel.offer.bank.utils.DataUtil;
import vn.com.viettel.offer.bank.utils.LogInfo;
import vn.com.viettel.offer.bank.utils.ResponseUtils;

@RestController
@RequestMapping("/api/offerBank")
public class OfferBankResource {
	
	Logger log = LoggerFactory.getLogger(OfferBankResource.class);
	
	public static final String PREFIX_ISDN = "isdn_";
	
	@Autowired
	private UserServiceImpl userService;

	@Autowired
	private StringRedisTemplate stringRedisTemplate;
    

	@Autowired
	MessageSource messageSource;
	
	@Autowired
	private PropertiesConfig config;
	
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	
	@PostMapping("/getBestOfferByISDN")
	@LogInfo(action = LogAction.READ, comment = "getBestOfferByISDN")
	public ResponseEntity<BaseResponse<Object>> getBestOfferByISDN(Locale locale, @RequestBody UserDTO userDTO){
		try {
			//Validate User
			if(!userService.checkUser(userDTO)) {
				return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR, messageSource.getMessage("user.incorrect", null, locale), null));
			};
			
			//validate list ISDN 
			if(!DataUtil.isNullOrEmpty(validateListISDNs(userDTO.getIsdns()))) {
				return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR, messageSource.getMessage("data.incorrect", null, locale), null));
			};
			// lấy thông tin từ redis
			
			Map<String, String> isdnAndSub = new HashMap<>();
			for (String it : userDTO.getIsdns()) {
				// lấy thông tin isdn và subid
				String isdnAndSubId = stringRedisTemplate.opsForValue().get(PREFIX_ISDN + it);
				isdnAndSub.put(it, isdnAndSubId);
			}
			Map<String, List<String>> mapOffer = new HashMap<>();
			List<String> listNoOfferId = Lists.newArrayList();
			
			// tìm kiếm thông tin bestoffer
			for(Map.Entry<String, String> entry : isdnAndSub.entrySet()) {
			    String offerInfo = stringRedisTemplate.opsForValue().get(entry.getValue());
			    if(!DataUtil.isNullOrEmpty(offerInfo)) {
			    	String offerId = null;
			    	try {
			    		offerId = offerInfo.split("\\|")[1];
					} catch (Exception e) {
						log.error("Sai định dạng thông tin lấy offer bằng subId: " + offerInfo);
					}
			    	if(offerId != null) {
			    		List<String> listIsdnHasOffer = Lists.newArrayList();
			    		if(mapOffer.containsKey(offerId)) {
			    			listIsdnHasOffer = mapOffer.get(offerId);
			    		}
			    		listIsdnHasOffer.add(entry.getKey() + "|" + entry.getValue());
			    		mapOffer.put(offerId, listIsdnHasOffer);
			    	}
			    }else {
			    	// không có best offer 
			    	listNoOfferId.add(entry.getKey() + "|" + entry.getValue());
			    }
			}
			
			//trả về thông tin kết quả
			BestOfferByISDNResponse res = new BestOfferByISDNResponse();
			// thông tin những thuê bao k có offer
			res.setListNoOfferId(Joiner.on(",").skipNulls().join(listNoOfferId));
			// thông tin những thuê bao có offer
			List<InfoByOfferId> arrayListInfoByofferId = Lists.newArrayList();
			for(Map.Entry<String, List<String>> entry : mapOffer.entrySet()) {
				InfoByOfferId it = new InfoByOfferId();
				it.setOfferId(entry.getKey());
				if(entry.getValue() != null) {
					it.setListIsdnAndSubId(Joiner.on(",").skipNulls().join(entry.getValue()));
				}
				arrayListInfoByofferId.add(it);
			}
			res.setArrayListInfoByOfferId(arrayListInfoByofferId);
			
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_SUCCESS, "Success", res));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_SYSTEM_ERR, messageSource.getMessage("app.internal.error", null, locale), null));
		}
	}


	private String validateListISDNs(List<String> isdNs) {
		try {
			if(isdNs == null) {
				return "EMPTY";
			}
			for (String string : isdNs) {
				if(string.startsWith("84") || string.startsWith("0")) {
					return string;
				}
				if(!string.matches("[0-9]+")) {
					return string;
				}
			}
			
			return null;
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}
	
	@PostMapping("/getBestOfferBySubID")
	@LogInfo(action = LogAction.READ, comment = "getBestOfferBySubID")
	public ResponseEntity<BaseResponse<Object>> getBestOfferBySubID(Locale locale, @RequestBody UserDTO userDTO){
		try {
			//Validate User
			if(!userService.checkUser(userDTO)) {
				return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR, messageSource.getMessage("user.incorrect", null, locale), null));
			};
			// lấy thông tin từ redis
			Map<String, List<String>> mapOffer = new HashMap<>();
			List<String> listNoOfferId = Lists.newArrayList();
			
			// tìm kiếm thông tin bestoffer
			for(String it : userDTO.getSubids()) {
				if(it == null || !it.matches("[0-9]+")) {
					return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR, messageSource.getMessage("data.incorrect", null, locale), null));
				}
			    String offerInfo = stringRedisTemplate.opsForValue().get(it);
			    if(!DataUtil.isNullOrEmpty(offerInfo)) {
			    	String offerId = null;
			    	try {
			    		offerId = offerInfo.split("\\|")[1];
					} catch (Exception e) {
						log.error("Sai định dạng thông tin lấy offer bằng subId: " + offerInfo);
					}
			    	if(offerId != null) {
			    		List<String> listIsdnHasOffer = Lists.newArrayList();
			    		if(mapOffer.containsKey(offerId)) {
			    			listIsdnHasOffer = mapOffer.get(offerId);
			    		}
			    		listIsdnHasOffer.add("null|" + it);
			    		mapOffer.put(offerId, listIsdnHasOffer);
			    	}
			    }else {
			    	// không có best offer 
			    	listNoOfferId.add("null|" + it);
			    }
			}
			
			//trả về thông tin kết quả
			BestOfferBySubIdResponse res = new BestOfferBySubIdResponse();
			// thông tin những thuê bao k có offer
			res.setListNoOfferId(Joiner.on(",").skipNulls().join(listNoOfferId));
			// thông tin những thuê bao có offer
			List<BestOfferBySubIdResponse.InfoByOfferId> arrayListInfoByofferId = Lists.newArrayList();
			for(Map.Entry<String, List<String>> entry : mapOffer.entrySet()) {
				BestOfferBySubIdResponse.InfoByOfferId it = new BestOfferBySubIdResponse.InfoByOfferId();
				it.setOfferId(entry.getKey());
				if(entry.getValue() != null) {
					it.setListIsdnAndSubId(Joiner.on(",").skipNulls().join(entry.getValue()));
				}
				arrayListInfoByofferId.add(it);
			}
			res.setArrayListInfoByOfferId(arrayListInfoByofferId);
			
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_SUCCESS, "Success", res));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_SYSTEM_ERR, messageSource.getMessage("app.internal.error", null, locale), null));
		}
	}
	
	
	@PostMapping("/getSegmentOfOffer")
	@LogInfo(action = LogAction.READ, comment = "getSegmentOfOffer")
	public ResponseEntity<BaseResponse<Object>> getSegmentOfOffer(Locale locale, @RequestBody UserDTO userDTO, HttpServletRequest  request){
		try {
			
			HttpServletRequest rq = request;
			int x = rq.getContentLength();
			
			System.out.println("Đã gọi vào hàm");
			//Validate User
			if(!userService.checkUser(userDTO)) {
				return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR, messageSource.getMessage("user.incorrect", null, locale), null));
			};
			
			//validate offerId
			if(userDTO.getOfferId() == null || !userDTO.getOfferId().matches("[0-9]+")) {
				return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR, messageSource.getMessage("data.incorrect", null, locale), null));
			}
			
			// -------------------- START CALL SPARK JOB --------------------
			
			System.out.println("Call spark job");
			String className = config.getClassName();
			String jarFile = config.getJarFile();
									
			String requestId = String.valueOf(new Random().nextInt(1000000000));
			
//			String[] args = new String[]{userDTO.getOfferId(), requestId};
//			SparkCommandEnvelope cmdEnve = new SparkCommandEnvelope(className,jarFile,args);
//				
//			String spark_host = config.getHost();
//			int spark_port = Integer.parseInt(config.getPort());
//			int timeOut = Integer.parseInt(config.getTimeOut());
//			
//			String result = new SparkCommandPublisher(spark_host, spark_port, timeOut).publish(cmdEnve);
//			
//			if("sparkError".equals(result)) {
//				return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_SYSTEM_ERR, messageSource.getMessage("spark.error", null, locale), null));
//			}
			
			// -------------------- END CALL SPARK JOB --------------------
			
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_SUCCESS, "Success", new SegmentOfOfferResponse(requestId)));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_SYSTEM_ERR, messageSource.getMessage("app.internal.error", null, locale), null));
		}
	
	}
	
}
